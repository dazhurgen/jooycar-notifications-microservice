import express from 'express';
import bodyParser, {json} from 'body-parser';
import mongoose from 'mongoose';

import { notificationsRouter } from "./routers/notifications.router";
const path = require("path");
const app = express()
app.use(json())
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/api/notifications/v1", notificationsRouter); 

app.listen(3001, ()=>{
    console.log('server listening on port 3001');
})