import express, { Request, Response } from "express";
 export const notificationsRouter = express.Router();
 import {sendNotificationAction} from '../controllers/actions.controller';
    
 notificationsRouter.post("/actions", async (req: Request, res: Response) => {
    console.log('requested to send notification');
    try{
        let result = await sendNotificationAction(req.body);
        res.send({result});
    }
    catch(error){
        console.log(error);
    }
    
})